//
//  GuessTheFlagApp.swift
//  GuessTheFlag
//
//  Created by Zac Mazza on 8/31/21.
//

import SwiftUI

@main
struct GuessTheFlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
